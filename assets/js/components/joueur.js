// gestion des jouers et leur affichage dans le jeu

export let monpersonnage = "";

export function prenom() {
    let nom = document.getElementById('myname').value;
    monpersonnage = nom;
    document.getElementById('nev').innerHTML = nom;
    document.getElementById('nevem').innerHTML = nom;
    document.getElementById('nevemperte').innerHTML = nom;

}

//recuperation le prenom pour le score

export function getPrenom() {
    let name = document.getElementById('myname').value;
    //console.log(name);
    return name;
}
// recuperation de l'avatar choisi dans l'affiche de jeu

export function avatar() {
    let valeur = document.querySelector('input[name=avatar]:checked').value;
    //console.log(valeur);
    let img = document.createElement("img");
    if (valeur == "avatar1") {
        img.src = "assets/image/avatar/avatar1.png";
    } else if (valeur == "avatar2") {
        img.src = "assets/image/avatar/avatar2.png";
    } else if (valeur == "avatar3") {
        img.src = "assets/image/avatar/avatar3.png";
    } else if (valeur == "avatar4") {
        img.src = "assets/image/avatar/avatar4.png";
    }
    img.style.width = "150px";
    let div = document.getElementById("chosenavatar");
    div.appendChild(img);
}