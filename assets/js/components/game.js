// on cré le coeur / moteur de notre jeu 

export let engine = Engine.create();

engine.world.gravity.y = 0;
export let render = Render.create({
    element: document.getElementById("myCanvas"),
    engine: engine,
    options: {
        width: 1000,
        height: 600,
        wireframes: false

    }
});

//pour mieux gerer le temps nous avons ajouter le 'render' - fin jeu, pause

Render.run(render);
export let runner = Runner.create();
Runner.run(runner, engine);

export function pauseGame() {
    Runner.stop(runner);
}

export function resumeGame() {
    Runner.run(runner, engine);
}