import {setBall, getnbBall, getArrayBall} from "./ballon.js";
import {engine, runner} from "./game.js";
import {getTimeRemaining} from "./time.js";
import {getHighScore, getScore, storeToLocal} from "./score.js";

//verifie si il y a toujours un ballon dans le monde vient enlever un ballon s'il sors du monde
//si le nombre de ballon arrive à 0 où qu'il n'y a plus de temps
//execute la fin du jeu
//on vient apres l'arret du jeu stocker le score du joueur
//en localstorage puis on affiche l'ecran de fin correspondant

export function gameOverBall() {
    Matter.Events.on(engine, 'beforeUpdate', function () {
        if (getnbBall() >= 1) {
            //console.log(getArrayBall());
            let tabBall = getArrayBall();
            for (let i = 0; i < tabBall.length; i++) {
                let oneball = tabBall[i];
                if (tabBall[i].position.y > 620) {
                    tabBall.splice(i, 1);
                    setBall();
                    World.remove(engine.world, oneball);
                }
            }
        }
        if ((getnbBall() == 0) || (getTimeRemaining() <= 0)) {
            let finalscore = getScore();
            document.getElementById('pont').innerText = finalscore;
            storeToLocal();
            getHighScore();
            Runner.stop(runner, engine);
            document.querySelector('.signin').style.display = 'none';
            document.querySelector('.affiche').style.display = 'none';
            document.querySelector('.dernier').style.display = 'block';
            document.querySelector('.over').style.display = 'block';
            document.querySelector('.perdre').style.display = 'block';
            document.querySelector('.rejouer').style.display = 'block';
            document.querySelector('.tableauScore').style.display = 'block';
        }

    });
}