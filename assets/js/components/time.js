import {runner, engine} from "./game.js";


//créer un compt à rebours secondes

export let currentTime =0;
let duration = 60;
let oui;
let tempsrestant;

//lance le timer toute les seconde

export function startTimer(duration, display) {
    oui = setInterval(intervalTimer, 1000);
}

//permet d'ajouter du temps dans le timer

export function addDuration(duree){
    duration += duree;
}

//premet d'enlever du temps dans le timer

export function removeDuration(duree){
    duration -= duree;
}

//vient s'excetuter dans le stratTimer toute les seconde pour afficher le temps restant

function intervalTimer(){
    currentTime++;
    tempsrestant = duration -currentTime;
    document.querySelector('#time').textContent = tempsrestant+"s";
    document.querySelector('#time').setAttribute("data-time", tempsrestant);
}

//permet de mettre le timer en pause

export function pauseTimer(){
   //console.log("pause");
    clearInterval(oui);
}

//permet de recuperer le temps restant dans le timer

export function getTimeRemaining(){
    let data = parseInt(document.querySelector('#time').getAttribute("data-time"));
    return data;
}

//gestion de la pause deu jeu
let paused = false;

//quand on click sur le bouton pause, met le jeu en pause
//quand on appuie sur echap met le jeu en pause, ou reprend la partie ainsi que le timer en cours si elle etait sur pause

let elm = document.querySelector("#pause");
elm.addEventListener('click', onPause);

document.addEventListener('keydown', event => { //sur echap pause et relance du jeu
    if (event.code === 'Escape') {
        if(paused == false){
            onPause();
            paused = true;
        }
        else{
            Runner.run(runner, engine);
            startTimer();
            paused = false;
            //console.log("pause2", paused)
        }
    }
});

//vient mettre sur pause le jeu ainsi que le timer

function onPause(){
    paused = true;
    Runner.stop(runner, engine);
    pauseTimer();
    //console.log("pause", paused);
}

//quand on click sur le bouton relance le jeu ainsi que le timer

let elm2 = document.querySelector("#relance");
elm2.addEventListener('click', (event) => {
    if(paused == true){
        Runner.run(runner, engine);
        startTimer();
        paused = false;
        //console.log("pause", paused)
    }
});


