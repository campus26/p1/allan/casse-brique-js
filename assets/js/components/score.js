import {getPrenom} from "./joueur.js";
import {getTimeRemaining} from "./time.js";

let score = 0;

//permet d'ajouter un score donne en parametre ou 1 par defaut

export function addscore(numscore) {
    if (numscore != null) {
        score += numscore;
    } else {
        score += 1;
    }
    document.getElementById("score").innerHTML = score;
}

//transforme le temps restant lors d'une victoire en score et l'ajoute au score

export function endGameScore() {
    let scoreTiming = getTimeRemaining();
    score += scoreTiming;
}

//retourne le score

export function getScore() {
    //console.log(score);
    return score;
}

//permet de stocker dans le localstorage le score du joueur, son prenom, ainsi que son numero de partie unique

export function storeToLocal() {

    let storedscore = JSON.parse(localStorage.getItem('BreakBrick'));

    if (storedscore === null) {
        storedscore = [];
    }
    let lastgame = (storedscore.length);
    storedscore.push({
        Player: {
            nom: getPrenom(),
            score: getScore(),
            partie: lastgame
        }
    });

    localStorage.setItem('BreakBrick', JSON.stringify(storedscore));
}

//realise un tri en fonction du meilleur score

export function getHighScore() {
    let highScorearray = JSON.parse(localStorage.getItem('BreakBrick'));
    let player = [];

    if (highScorearray === null) {
        return null;
    } else {
        for (let i = 0; i < highScorearray.length; i++) {
            player.push(highScorearray[i].Player);
        }
    }

    let val = player[player.length - 1];

    player.sort(function compare(a, b) {
        if (a.score > b.score)
            return -1;
        if (a.score < b.score)
            return 1;
        return 0;
    });
    let playerTop10 = player.slice(0, 10);

//stock les 10 meilleurs score et les affiches dans un tableau

    function affichePlayerResult(element, index, array) {
        let ligne = document.createElement("tr");
        let nindex = index + 1;
        ligne.innerHTML += "<td>" + nindex + "</td>";
        ligne.innerHTML += "<td>" + element.nom + "</td>";
        ligne.innerHTML += "<td>" + element.score + "</td>";
        document.querySelector("#highscore").appendChild(ligne);
    }
    playerTop10.forEach(affichePlayerResult);

//donne le classement de la partie que le joueur vient d'executer

    function afficheLastPlayerResult(element, index, array) {
        if (element.partie === val.partie) {

            let ligne = document.createElement("tr");
            let nindex = index + 1;
            let nbjoueur = element.partie + 1;
            ligne.innerHTML += nindex + "  sur  " + nbjoueur + " joueurs";
            document.querySelector("#classement").appendChild(ligne);
        }
    }
    player.forEach(afficheLastPlayerResult);
}